<?php

declare(strict_types=1);

namespace App\Response;

final class Response
{
    private string $currencyCode;

    private string $baseCurrencyCode;

    private string $date;

    private string $rate;

    private string $difference;

    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode(string $currencyCode): self
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    public function getBaseCurrencyCode(): string
    {
        return $this->baseCurrencyCode;
    }

    public function setBaseCurrencyCode(string $baseCurrencyCode): self
    {
        $this->baseCurrencyCode = $baseCurrencyCode;

        return $this;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getRate(): string
    {
        return $this->rate;
    }

    public function setRate(string $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getDifference(): string
    {
        return $this->difference;
    }

    public function setDifference(string $difference): self
    {
        $this->difference = $difference;

        return $this;
    }
}
