<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Rate;
use App\Enum\Currency;
use App\Message\RateLoading;
use App\Repository\DbRateRepository;
use App\Repository\RateRepositoryInterface;
use DateInterval;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Serializer\SerializerInterface;

#[AsCommand(name: 'app:load-rates')]
final class LoadRatesCommand extends Command
{
    private const BATCH_SIZE = 50;

    private DateTimeImmutable $dateFrom;

    private DateTimeImmutable $dateTo;

    private RateRepositoryInterface $rateRepository;

    private ProducerInterface $producer;

    private SerializerInterface $serializer;

    private EntityManagerInterface $entityManager;

    public function __construct(
        #[Autowire(service: DbRateRepository::class)]
        RateRepositoryInterface $rateRepository,
        ProducerInterface $producer,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
        string $name = null
    ) {
        parent::__construct($name);

        $this->rateRepository = $rateRepository;
        $this->producer = $producer;
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this->setDescription('Load exchange rates')
            ->addOption('dateFrom', null, InputOption::VALUE_OPTIONAL, 'Date from (Y-m-d)')
            ->addOption('dateTo', null, InputOption::VALUE_OPTIONAL, 'Date to (Y-m-d)');
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->dateFrom = $input->getOption('dateFrom') ?
            DateTimeImmutable::createFromFormat('Y-m-d', $input->getOption('dateFrom'))->setTime(0, 0) :
            new DateTimeImmutable('-180 day');

        $this->dateTo = $input->getOption('dateTo') ?
            DateTimeImmutable::createFromFormat('Y-m-d', $input->getOption('dateTo'))->setTime(23, 23, 59) :
            new DateTimeImmutable('today 1 sec ago');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $i = 1;
        foreach (Currency::cases() as $currency) {
            $rate = $this->rateRepository->getLastRateByCurrencyId($currency);

            $this->defineDateFrom($rate);

            $message = $this->createMessage($currency);

            $this->producer->publish($this->serializer->serialize($message, 'json'));

            if ($i % self::BATCH_SIZE === 0) {
                $this->entityManager->clear();
            }
            $i++;
        }

        return Command::SUCCESS;
    }

    private function defineDateFrom(?Rate $rate): void
    {
        if ($rate) {
            $rateDateFrom = DateTimeImmutable::createFromFormat('Y-m-d', $rate->getDate());
            $rateDateFrom = $rateDateFrom->add(DateInterval::createFromDateString('1 day'));
            $this->dateFrom = $rateDateFrom <= $this->dateTo ? $rateDateFrom : $this->dateTo;
        }
    }

    private function createMessage(Currency $currency): RateLoading
    {
        return
            (new RateLoading())
                ->setUniqueId($currency->value)
                ->setDateTo($this->dateTo->format('d/m/Y'))
                ->setDateFrom($this->dateFrom->format('d/m/Y'))
            ;
    }
}
