<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Rate as RateEntity;
use App\Enum\Currency;
use App\Service\Dto\Rate;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class DbRateRepository extends ServiceEntityRepository implements RateRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RateEntity::class);
    }

    public function getByCurrencyCodeAndDate(DateTimeImmutable $date, string $currencyCode): Rate
    {
        return new Rate();
    }

    public function getLastRateByCurrencyId(Currency $currency): ?RateEntity
    {
        $qb = $this->createQueryBuilder('r');

        $qb->where('r.uniqueId = :uniqueId')
            ->orderBy('r.date', 'desc')
            ->setMaxResults(1)
        ;

        $qb->setParameter(':uniqueId', $currency->value);

        return $qb->getQuery()->getOneOrNullResult();
    }
}
