<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Rate as RateEntity;
use App\Enum\Currency;
use App\Exception\NotFoundException;
use App\Service\Dto\Rate;
use App\Service\Extractor\ExtractorInterface;
use DateTimeImmutable;
use SimpleXMLElement;

final class RateRepository implements RateRepositoryInterface
{
    private ExtractorInterface $extractor;

    public function __construct(ExtractorInterface $extractor)
    {
        $this->extractor = $extractor;
    }

    public function getByCurrencyCodeAndDate(DateTimeImmutable $date, string $currencyCode): Rate
    {
        $xml = $this->extractor->extractDaily($date);
        $result = $xml->xpath('Valute[CharCode="'.$currencyCode.'"]');

        if (count($result) === 0) {
            throw new NotFoundException();
        }

        return $this->createRate(current($result));
    }

    private function createRate(SimpleXMLElement $simpleXMLElement): Rate
    {
        return
            (new Rate())
                ->setCurrencyCode((string) $simpleXMLElement->CharCode)
                ->setNumberOCode((string) $simpleXMLElement->NumCode)
                ->setNominal((string) $simpleXMLElement->Nominal)
                ->setName((string) $simpleXMLElement->Name)
                ->setValue((string) $simpleXMLElement->Value)
            ;
    }

    public function getLastRateByCurrencyId(Currency $currency): ?RateEntity
    {
        /*_*/
    }
}
