<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Rate as RateEntity;
use App\Enum\Currency;
use App\Service\Dto\Rate;
use DateTimeImmutable;

interface RateRepositoryInterface
{
    public function getByCurrencyCodeAndDate(DateTimeImmutable $date, string $currencyCode): Rate;

    public function getLastRateByCurrencyId(Currency $currency): ?RateEntity;
}
