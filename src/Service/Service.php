<?php

declare(strict_types=1);

namespace App\Service;

use App\Response\Response;
use App\Service\Dto\Rate;
use App\Service\Resolver\ResolverInterface;
use DateInterval;
use DateTimeImmutable;
use App\Request\Dto\GetRateRequest;

final class Service implements ServiceInterface
{
    private ResolverInterface $resolver;

    public function __construct(
        ResolverInterface $resolver
    ) {
        $this->resolver = $resolver;
    }

    public function get(GetRateRequest $request): Response
    {
        $date = DateTimeImmutable::createFromFormat('d/m/Y', $request->getDate());
        $previousDate = DateTimeImmutable::createFromFormat('d/m/Y', $request->getDate())->sub(DateInterval::createFromDateString('1 day'));

        $requestDayRate = $this->resolver->resolver($request)->calculate($date, $request);
        $previousDayRate = $this->resolver->resolver($request)->calculate($previousDate, $request);

        return $this->createResponse($request, $requestDayRate->getValue(), $this->getDifference($requestDayRate, $previousDayRate));
    }

    private function createResponse(GetRateRequest $request, string $rate, string $difference): Response
    {
        return
            (new Response())
                ->setDate($request->getDate())
                ->setBaseCurrencyCode($request->getBaseCurrencyCode())
                ->setCurrencyCode($request->getCurrencyCode())
                ->setRate($rate)
                ->setDifference($difference)
            ;
    }

    private function getDifference(Rate $requestDayRate,Rate $previousDayRate): string
    {
        $difference = (float) $requestDayRate->getValue() - (float) $previousDayRate->getValue();

        return (string) round($difference, 5);
    }
}
