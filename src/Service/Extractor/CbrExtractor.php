<?php

declare(strict_types=1);

namespace App\Service\Extractor;

use App\Message\RateLoading;
use DateTimeImmutable;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Psr\Log\LoggerInterface;
use SimpleXMLElement;
use Throwable;

final class CbrExtractor implements ExtractorInterface
{
    private LoggerInterface $logger;

    private string $baseUrl;

    private Client $client;

    public function __construct(
        LoggerInterface $logger,
        string $baseUrl
    ) {
        $this->logger = $logger;
        $this->baseUrl = $baseUrl;
        $this->client = new Client();
    }

    public function extractDaily(DateTimeImmutable $date): SimpleXMLElement
    {
        $uri = sprintf('%s/scripts/XML_daily.asp?date_req=%s', $this->baseUrl, $date->format('d/m/Y'));
        $request = new Request('GET', $uri);

        try {
            $response = $this->client->send($request);

            return new SimpleXMLElement($response->getBody()->getContents());
        } catch (Throwable $exception) {
            $this->logger->error($exception->getMessage());
            /* Тут следует какая то обработка исключения*/

            throw $exception;
        }
    }

    public function extractRatesByUniqueIdAndDate(RateLoading $rateLoading): SimpleXMLElement
    {
        $uri = sprintf(
            '%s/scripts/XML_dynamic.asp?date_req1=%s&date_req2=%s&VAL_NM_RQ=%s',
            $this->baseUrl,
            $rateLoading->getDateFrom(),
            $rateLoading->getDateTo(),
            $rateLoading->getUniqueId(),
        );
        $request = new Request('GET', $uri);

        try {
            $response = $this->client->send($request);

            return new SimpleXMLElement($response->getBody()->getContents());
        } catch (Throwable $exception) {
            $this->logger->error($exception->getMessage());
            /* Тут следует какая то обработка исключения*/

            throw $exception;
        }
    }
}
