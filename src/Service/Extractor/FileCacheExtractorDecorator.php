<?php

declare(strict_types=1);

namespace App\Service\Extractor;

use App\Message\RateLoading;
use DateTimeImmutable;
use SimpleXMLElement;
use Symfony\Component\DependencyInjection\Attribute\AsDecorator;

#[AsDecorator(decorates: CbrExtractor::class)]
final class FileCacheExtractorDecorator implements ExtractorInterface
{
    private ExtractorInterface $inner;

    private string $cacheFile;

    private int $cacheTimeOut;

    public function __construct(
        ExtractorInterface $inner,
        string $cacheFile,
        int $cacheTimeOut
    ) {
        $this->inner = $inner;
        $this->cacheFile = $cacheFile;
        $this->cacheTimeOut = $cacheTimeOut;
    }

    public function extractDaily(DateTimeImmutable $date): SimpleXMLElement
    {
        $file = sprintf('%s/rateDate%s.xml', $this->cacheFile, $date->format('Ymd'));

        if(!is_file($file) || filemtime($file) < (time() - $this->cacheTimeOut)) {
            $result = $this->inner->extractDaily($date);

            file_put_contents($file, $result->asXML());
        }

        return simplexml_load_file($file);
    }

    public function extractRatesByUniqueIdAndDate(RateLoading $rateLoading): SimpleXMLElement
    {
        return $this->inner->extractRatesByUniqueIdAndDate($rateLoading);
    }
}
