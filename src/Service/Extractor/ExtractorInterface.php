<?php

declare(strict_types=1);

namespace App\Service\Extractor;

use App\Message\RateLoading;
use DateTimeImmutable;
use Exception;
use SimpleXMLElement;

interface ExtractorInterface
{
    public function extractDaily(DateTimeImmutable $date): SimpleXMLElement;
    public function extractRatesByUniqueIdAndDate(RateLoading $rateLoading): SimpleXMLElement;
}
