<?php

declare(strict_types=1);

namespace App\Service\Resolver;

use App\Service\RateCalculator\CalculatorInterface;
use App\Request\Dto\GetRateRequest;

interface ResolverInterface
{
    public function resolver(GetRateRequest $request): CalculatorInterface;
}
