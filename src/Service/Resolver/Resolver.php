<?php

declare(strict_types=1);

namespace App\Service\Resolver;

use App\Service\RateCalculator\CalculatorInterface;
use App\Service\RateCalculator\CrossRateCalculator;
use App\Service\RateCalculator\DefaultCalculator;
use App\Request\Dto\GetRateRequest;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

final class Resolver implements ResolverInterface
{
    private const BASE_CURRENCY = 'RUR';

    private CalculatorInterface $defaultCalculator;
    private CalculatorInterface $crossRateCalculator;

    public function __construct(
        #[Autowire(service: DefaultCalculator::class)]
        CalculatorInterface $defaultCalculator,
        #[Autowire(service: CrossRateCalculator::class)]
        CalculatorInterface $crossRateCalculator
    )
    {
        $this->defaultCalculator = $defaultCalculator;
        $this->crossRateCalculator = $crossRateCalculator;
    }

    public function resolver(GetRateRequest $request): CalculatorInterface
    {
        if ($request->getBaseCurrencyCode() === self::BASE_CURRENCY) {
            return $this->defaultCalculator;
        }

        return $this->crossRateCalculator;
    }
}
