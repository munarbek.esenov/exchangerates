<?php

declare(strict_types=1);

namespace App\Service\Dto;

final class Rate
{
    private string $currencyCode;

    private string $numberOCode;

    private string $nominal;

    private string $name;

    private string $value;

    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode(string $currencyCode): self
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    public function getNumberOCode(): string
    {
        return $this->numberOCode;
    }

    public function setNumberOCode(string $numberOCode): self
    {
        $this->numberOCode = $numberOCode;

        return $this;
    }

    public function getNominal(): string
    {
        return $this->nominal;
    }

    public function setNominal(string $nominal): self
    {
        $this->nominal = $nominal;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
