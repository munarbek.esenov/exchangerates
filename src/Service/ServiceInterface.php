<?php

declare(strict_types=1);

namespace App\Service;

use App\Response\Response;
use App\Request\Dto\GetRateRequest;

interface ServiceInterface
{
    public function get(GetRateRequest $request): Response;
}
