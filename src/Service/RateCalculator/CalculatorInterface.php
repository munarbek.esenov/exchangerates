<?php

declare(strict_types=1);

namespace App\Service\RateCalculator;

use App\Service\Dto\Rate;
use DateTimeImmutable;
use App\Request\Dto\GetRateRequest;

interface CalculatorInterface
{
    public function calculate(DateTimeImmutable $date, GetRateRequest $request): Rate;
}
