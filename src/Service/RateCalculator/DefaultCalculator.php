<?php

declare(strict_types=1);

namespace App\Service\RateCalculator;

use App\Repository\RateRepository;
use App\Repository\RateRepositoryInterface;
use App\Service\Dto\Rate;
use DateTimeImmutable;
use App\Request\Dto\GetRateRequest;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

final class DefaultCalculator implements CalculatorInterface
{
    private RateRepositoryInterface $rateRepository;

    public function __construct(
        #[Autowire(service: RateRepository::class)]
        RateRepositoryInterface $rateRepository
    ) {
        $this->rateRepository = $rateRepository;
    }

    public function calculate(DateTimeImmutable $date, GetRateRequest $request): Rate
    {
        return $this->rateRepository->getByCurrencyCodeAndDate($date, $request->getCurrencyCode());
    }
}
