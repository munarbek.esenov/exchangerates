<?php

declare(strict_types=1);

namespace App\Service\RateCalculator;

use App\Repository\RateRepository;
use App\Repository\RateRepositoryInterface;
use App\Service\Dto\Rate;
use DateTimeImmutable;
use App\Request\Dto\GetRateRequest;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

final class CrossRateCalculator implements CalculatorInterface
{
    const SCALE = 5;

    private RateRepositoryInterface $rateRepository;

    public function __construct(
        #[Autowire(service: RateRepository::class)]
        RateRepositoryInterface $rateRepository
    ) {
        $this->rateRepository = $rateRepository;
    }

    public function calculate(DateTimeImmutable $date, GetRateRequest $request): Rate
    {
        $targetCurrencyRate = $this->rateRepository->getByCurrencyCodeAndDate($date, $request->getCurrencyCode());
        $baseCurrencyRate = $this->rateRepository->getByCurrencyCodeAndDate($date, $request->getBaseCurrencyCode());
        $targetCurrencyValue = (float) $targetCurrencyRate->getValue() / $targetCurrencyRate->getNominal();
        $baseCurrencyValue = (float) $baseCurrencyRate->getValue() / $baseCurrencyRate->getNominal();
        $value = round($targetCurrencyValue / $baseCurrencyValue, self::SCALE);

        return $this->createRate($targetCurrencyRate, (string) $value);
    }

    private function createRate(Rate $targetCurrencyRate, string $value): Rate
    {
        return
            (new Rate())
                ->setCurrencyCode($targetCurrencyRate->getCurrencyCode())
                ->setValue($value)
            ;
    }
}
