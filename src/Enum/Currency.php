<?php

declare(strict_types=1);

namespace App\Enum;

enum Currency: string
{
    case USD = 'R01235';
    case AUD = 'R01010';
    case AZN = 'R01020A';
    case GBP = 'R01035';
    case AMD = 'R01060';
    case BYN = 'R01090B';
    case BGN = 'R01100';
    case BRL = 'R01115';
    case HUF = 'R01135';
    case HKD = 'R01200';
    case DKK = 'R01215';
    case EUR = 'R01239';
    case INR = 'R01270';
    case KZT = 'R01335';
    case CAD = 'R01350';
    case KGS = 'R01370';
    case CNY = 'R01375';
    case MDL = 'R01500';
    case NOK = 'R01535';
    case PLN = 'R01565';
    case RON = 'R01585F';
    case XDR = 'R01589';
    case SGD = 'R01625';
    case TJS = 'R01670';
    case TRY = 'R01700J';
    case TMT = 'R01710A';
    case UZS = 'R01717';
    case UAH = 'R01720';
    case CZK = 'R01760';
    case SEK = 'R01770';
    case CHF = 'R01775';
    case ZAR = 'R01810';
    case KRW = 'R01815';
    case JPY = 'R01820';

    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }

    public static function names(): array
    {
        return array_column(self::cases(), 'name');
    }
}
