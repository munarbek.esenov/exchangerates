<?php

declare(strict_types=1);

namespace App\Consumer;

use App\Entity\Rate;
use App\Enum\Currency;
use App\Message\RateLoading;
use App\Service\Extractor\CbrExtractor;
use App\Service\Extractor\ExtractorInterface;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Serializer\SerializerInterface;

final class RateLoadingConsumer
{
    private const BATCH_SIZE = 50;

    private SerializerInterface $serializer;

    private ExtractorInterface $extractor;

    private EntityManagerInterface $entityManager;

    public function __construct(
        SerializerInterface $serializer,
        #[Autowire(service: CbrExtractor::class)]
        ExtractorInterface $extractor,
        EntityManagerInterface $entityManager
    ) {
        $this->serializer = $serializer;
        $this->extractor = $extractor;
        $this->entityManager = $entityManager;
    }

    public function execute(AMQPMessage $msg)
    {
        $msg = $this->serializer->deserialize($msg->getBody(), RateLoading::class, 'json');

        $result = $this->extractor->extractRatesByUniqueIdAndDate($msg);
        $i = 0;

        foreach ($result->xpath('Record') as $record) {
            $date = DateTimeImmutable::createFromFormat('d.m.Y', (string) $record->attributes()->Date);
            $currency = Currency::from((string) $record->attributes()->Id);
            $rate = new Rate();
            $rate->setDate($date->format('Y-m-d'));
            $rate->setValue((string) $record->Value);
            $rate->setNominal((int) $record->Nominal);
            $rate->setUniqueId($currency->value);
            $rate->setUniqueCode($currency->name);

            $this->entityManager->persist($rate);
            if ($i % self::BATCH_SIZE) {
                $this->entityManager->flush();
                $this->entityManager->clear();
            }
        }
        $this->entityManager->flush();
        $this->entityManager->clear();
    }
}
