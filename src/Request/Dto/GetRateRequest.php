<?php

declare(strict_types=1);

namespace App\Request\Dto;

use Symfony\Component\Validator\Constraints as Assert;

final class GetRateRequest
{
    #[Assert\NotBlank]
    private string $currencyCode;

    private string $baseCurrencyCode = 'RUR';

    #[Assert\NotBlank]
    #[Assert\DateTime(format: 'd/m/Y', message: 'Некорректный формат даты. Правильный d/m/Y')]
    private string $date;

    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode(string $currencyCode): self
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    public function getBaseCurrencyCode(): string
    {
        return $this->baseCurrencyCode;
    }

    public function setBaseCurrencyCode(string $baseCurrencyCode): self
    {
        $this->baseCurrencyCode = $baseCurrencyCode;

        return $this;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }
}
