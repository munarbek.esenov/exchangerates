<?php

declare(strict_types=1);

namespace App\Request\Assembler;

use App\Request\Dto\GetRateRequest;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;

final class Assembler implements AssemblerInterface
{
    private SerializerInterface $serializer;

    private LoggerInterface $logger;

    public function __construct(
        SerializerInterface $serializer,
        LoggerInterface $logger
    ) {
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    public function asseble(Request $request): GetRateRequest
    {
        try {
            return $this->serializer->deserialize(
                $request->getContent(),
                GetRateRequest::class,
                'json'
            );
        } catch (Throwable $exception) {
            $this->logger->error($exception->getMessage(), [
                'exception' => $exception
            ]);

            throw new BadRequestException();
        }
    }
}
