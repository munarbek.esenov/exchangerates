<?php

declare(strict_types=1);

namespace App\Request\Assembler;

use App\Request\Dto\GetRateRequest;
use Symfony\Component\HttpFoundation\Request;

interface AssemblerInterface
{
    public function asseble(Request $request): GetRateRequest;
}
