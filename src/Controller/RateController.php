<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\NotFoundException;
use App\Request\Assembler\AssemblerInterface;
use App\Service\ServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class RateController extends AbstractController
{
    private AssemblerInterface $assembler;

    private ValidatorInterface $validator;

    private ServiceInterface $service;

    private NormalizerInterface $serializer;

    public function __construct(
        AssemblerInterface $assembler,
        ValidatorInterface $validator,
        ServiceInterface $service,
        NormalizerInterface $serializer
    ) {
        $this->assembler = $assembler;
        $this->validator = $validator;
        $this->service = $service;
        $this->serializer = $serializer;
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route('/rate', methods: ['POST'])]
    public function getRate(Request $request): JsonResponse
    {
        $rateRequest = $this->assembler->asseble($request);
        $errors = $this->validator->validate($rateRequest);

        if ($errors->count() > 0) {
            return $this->getValidationErrorMessages($errors);
        }

        try {
            $response = $this->service->get($rateRequest);
        } catch (NotFoundException $exception) {
            return new JsonResponse(["error" => 'Data not found'], Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($this->serializer->normalize($response));
    }

    private function getValidationErrorMessages(ConstraintViolationList $constraintViolationList): JsonResponse
    {
        $result = [];

        foreach ($constraintViolationList as $violation) {
            $result[$violation->getPropertyPath()] = $violation->getMessage();
        }

        return new JsonResponse($result, Response::HTTP_BAD_REQUEST);
    }
}
