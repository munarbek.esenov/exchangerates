<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230810071619 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE rates (
            id SERIAL PRIMARY KEY,
            date DATE NOT NULL,
            unique_code varchar(255) NOT NULL,
            unique_id varchar(255) NOT NULL,
            nominal INT NOT NULL,
            value varchar(255) NOT NULL
        )');

        $this->addSql('CREATE UNIQUE INDEX uidx__rates__date__unique_code ON rates (date, unique_code)');
        $this->addSql('CREATE INDEX idx__rates__unique_id ON rates (unique_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE rates');
    }
}
