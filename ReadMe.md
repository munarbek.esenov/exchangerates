1. Клонировать репу\
`git clone git@gitlab.com:munarbek.esenov/exchangerates.git`
2. Поднять контейнеры \
`docker-compose up -d`
3. Установить зависимости \
 `docker-compose exec exchange-rates-php-fpm composer install`
4. По апи можно получить курс валют с cbr \
   POST http://127.0.0.1:8060/rate \
   `{
   "currency_code": "EUR",
   "base_currency_code": "USD",
   "date": "11/06/2023"
   }`
5. Запуск консюмера \
 `docker-compose exec exchange-rates-php-fpm bin/console rabbitmq:consume rate_loading`
6. Запуск команды для выгрузки курсов за последние 180 дней \
 `docker-compose exec exchange-rates-php-fpm bin/console app:load-rates`
7. Вход в базу \
 `docker-compose exec exchange-rates-database psql -d database --username=user`
